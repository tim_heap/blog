from django.contrib import admin
from blog.models import Post, Series


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'published']

    prepopulated_fields = {'slug': ['title']}

    fieldsets = [
        (None, {
            'fields': [
                'title',
                'slug',
                'body_html'
            ]
        }),
        ('Meta', {
            'fields': ['published', 'public', 'series'],
        })
    ]
admin.site.register(Post, PostAdmin)


class SeriesAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']

    prepopulated_fields = {'slug': ['name']}
admin.site.register(Series, SeriesAdmin)
