from django.shortcuts import render, get_object_or_404

from blog.models import Post, Series


def index(request):
    posts = Post.objects.listed().prefetch_related('series')

    return render(request, 'blog/index.html', {
        'posts': posts,
    })


def view(request, slug):
    post = get_object_or_404(
        Post.objects.published().prefetch_related('series'), slug=slug)

    return render(request, 'blog/view.html', {
        'post': post
    })


def series(request, slug):
    series = get_object_or_404(Series.objects, slug=slug)
    posts = Post.objects.listed().filter(series=series)\
        .order_by('created').prefetch_related('series')

    return render(request, 'blog/series.html', {
        'series': series,
        'posts': posts,
    })


def archive(request, year=None, month=None, day=None):
    pass
