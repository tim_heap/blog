DEBUG = True

SITE_ID = 1

INTERNAL_IPS = (
    '127.0.0.1',
)

ASSETS_URL = '/assets/'
MEDIA_URL = ASSETS_URL + 'media/'
STATIC_URL = ASSETS_URL + 'static/'
