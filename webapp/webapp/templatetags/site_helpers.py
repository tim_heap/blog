import hashlib

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter()
@stringfilter
def to_code(string, length):
    digest = hashlib.md5(string).digest()
    code = reduce(lambda x, y: x ^ ord(y), digest, 0) % length
    return code
